﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using C1.Win.C1Ribbon;
using System.IO;
using C1.Win.C1Chart;

namespace SingleEconomic
{
    public partial class DataminingForm : C1RibbonForm
    {
        private const int CONST_MIDDLE_RATE = 10000;
        private const int CONST_INTERVAL_RATE = 10;

        private const int X_CONST_MIDDLE_RATE = 100000;
        private const int X_CONST_INTERVAL_RATE = 10;

        private const int Y_CONST_MIDDLE_RATE = 100000;
        private const int Y_CONST_INTERVAL_RATE = 10;

        private string file_directory;
        private string file_name;

        private string x_file_directory;
        private string x_file_name;

        private string y_file_directory;
        private string y_file_name;

        private int data_count;

        private int year_data;
        private int month_data;
        private int day_data;
        private int hour_data;
        private int min_data;
        private int sec_data;

        private List<double> x_series_data;
        private List<double> y_series_data;
        private List<double> series_data;

        private List<KeyValuePair<DateTime, double>> pair_data;
        private List<List<KeyValuePair<DateTime, double>>> dynamic_pair_data;

        private List<KeyValuePair<DateTime, double>> x_pair_data;
        private List<List<KeyValuePair<DateTime, double>>> x_dynamic_pair_data;

        private List<KeyValuePair<DateTime, double>> y_pair_data;
        private List<List<KeyValuePair<DateTime, double>>> y_dynamic_pair_data;

        private StreamReader file_reader;
        private StreamWriter file_writer;

        public DataminingForm()
        {
            InitializeComponent();
        }

        private List<KeyValuePair<DateTime, double>> pairExtract(int type)
        {
            List<KeyValuePair<DateTime, double>> return_data = new List<KeyValuePair<DateTime, double>>();
            DateTime insert_date_time;
            double convert;

            data_count = 0;

            
            string lineData;
            OpenFileDialog openDialog = new OpenFileDialog();

            openDialog.Filter = "Data File (*.txt)|*.txt";
            openDialog.Title = "데이터 파일을 로드합니다. 원하시는 파일을 선택해 주십시오.";

            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                if (type == 0)
                {
                    file_directory = openDialog.FileName;
                    file_name = openDialog.SafeFileName.Substring(0, openDialog.SafeFileName.Length - 4);
                }
                else if (type == 1)
                {
                    x_file_directory = openDialog.FileName;
                    x_file_name = openDialog.SafeFileName.Substring(0, openDialog.SafeFileName.Length - 4);
                }
                else
                {
                    y_file_directory = openDialog.FileName;
                    y_file_name = openDialog.SafeFileName.Substring(0, openDialog.SafeFileName.Length - 4);
                }

                return_data = new List<KeyValuePair<DateTime, double>>();

                if (File.Exists(openDialog.FileName))
                {
                    file_reader = new StreamReader(openDialog.FileName, System.Text.Encoding.UTF8);

                    while (!file_reader.EndOfStream)
                    {
                        lineData = file_reader.ReadLine();

                        string[] splitData = lineData.Split();

                        if (splitData.Length >= 7)
                        {
                            year_data = Convert.ToInt32(splitData[0]);
                            month_data = Convert.ToInt32(splitData[1]);
                            day_data = Convert.ToInt32(splitData[2]);
                            hour_data = Convert.ToInt32(splitData[3]);
                            min_data = Convert.ToInt32(splitData[4]);
                            sec_data = Convert.ToInt32(splitData[5]);

                            insert_date_time = new DateTime(year_data, month_data, day_data, hour_data, min_data, sec_data);

                            if (splitData[6] != "")
                                convert = Convert.ToDouble(splitData[6]);
                            else
                                convert = pair_data[data_count - 1].Value;

                            return_data.Add(new KeyValuePair<DateTime, double>(insert_date_time, convert));

                            data_count++;
                        }
                    }

                    file_reader.Close();
                }
            }

            return return_data;
        }

        private List<List<KeyValuePair<DateTime, double>>> classifyPiarData(List<KeyValuePair<DateTime, double>> input_pair_data)
        {
            List<List<KeyValuePair<DateTime, double>>> return_classify_data = new List<List<KeyValuePair<DateTime, double>>>();
            List<KeyValuePair<int, int>> month_list = new List<KeyValuePair<int, int>>();
            List<KeyValuePair<DateTime, double>> insert_temp_data;
            KeyValuePair<int, int> temp_pair;

            foreach (KeyValuePair<DateTime, double> data in input_pair_data)
            {
                temp_pair = new KeyValuePair<int, int>(data.Key.Year, data.Key.Month);

                if (month_list.Contains(temp_pair) == false)
                    month_list.Add(temp_pair);
            }

            for (int i = 0; i < month_list.Count; i++)
            {
                insert_temp_data = new List<KeyValuePair<DateTime, double>>();

                List<int> day_list = new List<int>();

                foreach (KeyValuePair<DateTime, double> data in input_pair_data)
                {
                    if (month_list[i].Value == data.Key.Month && day_list.Contains(data.Key.Day) == false)
                        day_list.Add(data.Key.Day);
                }

                foreach (int day in day_list)
                {
                    int init_hour = 9;
                    int init_min = 0;
                    int init_sec = 0;
                    DateTime temp_date;

                    for (int j = 0; j < 365; j++)
                    {
                        temp_date = new DateTime(month_list[i].Key, month_list[i].Value, day, init_hour, init_min, init_sec);
                        insert_temp_data.Add(new KeyValuePair<DateTime, double>(temp_date, 0));

                        init_min++;

                        if (init_min == 60)
                        {
                            init_min = 0;
                            init_hour++;
                        }
                    }

                    temp_date = new DateTime(month_list[i].Key, month_list[i].Value, day, 15, 15, 0);
                    insert_temp_data.Add(new KeyValuePair<DateTime, double>(temp_date, 0));
                }

                for (int j = 0; j < input_pair_data.Count; j++)
                {
                    for (int k = 0; k < insert_temp_data.Count; k++)
                    {
                        if (input_pair_data[j].Key.CompareTo(insert_temp_data[k].Key) == 0)
                        {
                            insert_temp_data[k] = new KeyValuePair<DateTime, double>(input_pair_data[j].Key, input_pair_data[j].Value);
                            break;
                        }
                    }
                }

                for (int j = 1; j < insert_temp_data.Count; j++)
                {
                    if (insert_temp_data[j].Value == 0.0)
                    {
                        insert_temp_data[j] = new KeyValuePair<DateTime, double>(insert_temp_data[j].Key, insert_temp_data[j - 1].Value);
                    }
                }

                return_classify_data.Add(insert_temp_data);
            }

            return return_classify_data;
        }

        private void createXYChart(string file_name, string x_label, string y_label)
        {
            xyChart.ChartGroups.Group0.ChartData.SeriesList.Clear();
            xyChart.ChartGroups.Group0.ChartType = C1.Win.C1Chart.Chart2DTypeEnum.XYPlot;

            xyChart.ChartArea.AxisX.Text = x_label;
            xyChart.ChartArea.AxisY.Text = y_label;


            ChartDataSeries cds_x = xyChart.ChartGroups.Group0.ChartData.SeriesList.AddNewSeries();
            cds_x.Label = "1";
            cds_x.LineStyle.Color = Color.Red;
            cds_x.LineStyle.Thickness = 1;
            cds_x.SymbolStyle.Size = 0;

            cds_x.X.CopyDataIn(series_data.ToArray());
            cds_x.Y.CopyDataIn(x_series_data.ToArray());

            ChartDataSeries cds_y = xyChart.ChartGroups.Group0.ChartData.SeriesList.AddNewSeries();
            cds_y.Label = "1";
            cds_y.LineStyle.Color = Color.Blue;
            cds_y.LineStyle.Thickness = 1;
            cds_y.SymbolStyle.Size = 0;

            cds_y.X.CopyDataIn(series_data.ToArray());
            cds_y.Y.CopyDataIn(y_series_data.ToArray());

            xyChart.SaveImage(file_name, System.Drawing.Imaging.ImageFormat.Png);
        }

        private void createTimePlotChart(string file_name, string x_label, string y_label)
        {
            xyChart.ChartGroups.Group0.ChartData.SeriesList.Clear();
            xyChart.ChartGroups.Group0.ChartType = C1.Win.C1Chart.Chart2DTypeEnum.XYPlot;

            xyChart.ChartArea.AxisX.Text = x_label;
            xyChart.ChartArea.AxisY.Text = y_label;


            xyChart.ChartArea.AxisX.UnitMajor = 20;
            xyChart.ChartArea.AxisY.UnitMajor = 20;
            xyChart.ChartArea.AxisX.UnitMinor = 10;
            xyChart.ChartArea.AxisX.UnitMinor = 10;

            //xyChart.ChartArea.AxisX.Min = -500;
            //xyChart.ChartArea.AxisX.Max = 500;
            //xyChart.ChartArea.AxisY.Min = -500;
            //xyChart.ChartArea.AxisY.Max = 500;

            ChartDataSeries cds = xyChart.ChartGroups.Group0.ChartData.SeriesList.AddNewSeries();
            cds.Label = "1";
            cds.LineStyle.Color = Color.Red;
            cds.LineStyle.Thickness = 1;
            cds.SymbolStyle.Size = 0;

            cds.X.CopyDataIn(x_series_data.ToArray());
            cds.Y.CopyDataIn(y_series_data.ToArray());

            xyChart.SaveImage(file_name, System.Drawing.Imaging.ImageFormat.Png);
        }

        private void file_open_btn_Click(object sender, EventArgs e)
        {
            pair_data = pairExtract(0);
        }

        private void month_classify_btn_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<int, int>> month_list = new List<KeyValuePair<int, int>>();
            List<KeyValuePair<DateTime, double>> insert_temp_data;
            KeyValuePair<int, int> temp_pair;

            foreach (KeyValuePair<DateTime, double> data in pair_data)
            {
                temp_pair = new KeyValuePair<int, int>(data.Key.Year, data.Key.Month);

                if (month_list.Contains(temp_pair) == false)
                    month_list.Add(temp_pair);
            }

            dynamic_pair_data = new List<List<KeyValuePair<DateTime, double>>>();

            for (int i = 0; i < month_list.Count; i++)
            {
                insert_temp_data = new List<KeyValuePair<DateTime, double>>();

                List<int> day_list = new List<int>();

                foreach (KeyValuePair<DateTime, double> data in pair_data)
                {
                    if (month_list[i].Value == data.Key.Month && day_list.Contains(data.Key.Day) == false)
                        day_list.Add(data.Key.Day);
                }

                foreach (int day in day_list)
                {
                    int init_hour = 9;
                    int init_min = 0;
                    int init_sec = 0;
                    DateTime temp_date;

                    for (int j = 0; j < 365; j++)
                    {
                        temp_date = new DateTime(month_list[i].Key, month_list[i].Value, day, init_hour, init_min, init_sec);
                        insert_temp_data.Add(new KeyValuePair<DateTime, double>(temp_date, 0));

                        init_min++;

                        if (init_min == 60)
                        {
                            init_min = 0;
                            init_hour++;
                        }
                    }

                    temp_date = new DateTime(month_list[i].Key, month_list[i].Value, day, 15, 15, 0);
                    insert_temp_data.Add(new KeyValuePair<DateTime, double>(temp_date, 0));
                }

                for (int j = 0; j < pair_data.Count; j++)
                {
                    for (int k = 0; k < insert_temp_data.Count; k++)
                    {
                        if (pair_data[j].Key.CompareTo(insert_temp_data[k].Key) == 0)
                        {
                            insert_temp_data[k] = new KeyValuePair<DateTime, double>(pair_data[j].Key, pair_data[j].Value);
                            break;
                        }
                    }
                }

                for (int j = 1; j < insert_temp_data.Count; j++)
                {
                    if (insert_temp_data[j].Value == 0.0)
                    {
                        insert_temp_data[j] = new KeyValuePair<DateTime, double>(insert_temp_data[j].Key, insert_temp_data[j - 1].Value);
                    }
                }

                dynamic_pair_data.Add(insert_temp_data);
            }
        }

        private void redata_btn_Click(object sender, EventArgs e)
        {
            int compare_count;

            for (int i = 0; i < dynamic_pair_data.Count; i++)
            {
                for (int j = 0; j < dynamic_pair_data.Count; j++)
                {
                    series_data = new List<double>();
                    x_series_data = new List<double>();
                    y_series_data = new List<double>();

                    compare_count = dynamic_pair_data[i].Count < dynamic_pair_data[j].Count ? dynamic_pair_data[i].Count : dynamic_pair_data[j].Count;

                    for (int k = 0; k < compare_count; k++)
                    {
                        series_data.Add(k);
                        x_series_data.Add(dynamic_pair_data[i][k].Value);
                        y_series_data.Add(dynamic_pair_data[j][k].Value);
                    }

                    int x_year = dynamic_pair_data[i][0].Key.Year;
                    int x_month = dynamic_pair_data[i][0].Key.Month;

                    int y_year = dynamic_pair_data[j][0].Key.Year;
                    int y_month = dynamic_pair_data[j][0].Key.Month;

                    string file_format = file_name + "-" + x_year + "-" + x_month + "&" + y_year + "-" + y_month;
                    createXYChart(file_format + ".png", "X " + x_year + "." + x_month, "Y " + y_year + "." + y_month);

                    file_writer = File.CreateText(file_format + ".txt");

                    for (int k = 0; k < compare_count; k++)
                    {
                        file_writer.WriteLine(x_series_data[k] + " " + y_series_data[k]);
                    }

                    file_writer.Close();
                }
            }
        }

        private void random_walk_btn_Click(object sender, EventArgs e)
        {
            int compare_count;
            
            for (int i = 0; i < dynamic_pair_data.Count; i++)
            {
                for (int j = 0; j < dynamic_pair_data.Count; j++)
                {
                    x_series_data = new List<double>();
                    y_series_data = new List<double>();

                    List<double> x_data = new List<double>();
                    List<double> y_data = new List<double>();

                    compare_count = dynamic_pair_data[i].Count < dynamic_pair_data[j].Count ? dynamic_pair_data[i].Count : dynamic_pair_data[j].Count;

                    for (int k = 0; k < compare_count; k++)
                    {
                        x_data.Add(dynamic_pair_data[i][k].Value);
                        y_data.Add(dynamic_pair_data[j][k].Value);
                    }

                    int x_position = 0;
                    int y_position = 0;

                    x_series_data.Add(x_position);
                    y_series_data.Add(y_position);

                    for (int k = 0; k < compare_count - 1; k++)
                    {
                        int x_rate = 0;
                        int y_rate = 0;

                        if (x_data[k] == 0.0)
                        {
                            x_rate = CONST_MIDDLE_RATE;
                        }
                        else
                        {
                            x_rate = (int)((x_data[k + 1] / x_data[k]) * CONST_MIDDLE_RATE);
                        }

                        if (y_data[k] == 0.0)
                        {
                            y_rate = CONST_MIDDLE_RATE;
                        }
                        else
                        {
                            y_rate = (int)((y_data[k + 1] / y_data[k]) * CONST_MIDDLE_RATE);
                        }

                        if (x_rate <= CONST_MIDDLE_RATE - CONST_INTERVAL_RATE)
                        {
                            x_position--;
                        }
                        else if (x_rate >= CONST_MIDDLE_RATE + CONST_INTERVAL_RATE)
                        {
                            x_position++;
                        }

                        if (y_rate <= CONST_MIDDLE_RATE - CONST_INTERVAL_RATE)
                        {
                            y_position--;
                        }
                        else if (y_rate >= CONST_MIDDLE_RATE + CONST_INTERVAL_RATE)
                        {
                            y_position++;
                        }

                        x_series_data.Add(x_position);
                        y_series_data.Add(y_position);
                    }

                    int x_year = dynamic_pair_data[i][0].Key.Year;
                    int x_month = dynamic_pair_data[i][0].Key.Month;

                    int y_year = dynamic_pair_data[j][0].Key.Year;
                    int y_month = dynamic_pair_data[j][0].Key.Month;

                    string file_format = file_name + "-" + x_year + "-" + x_month + "&" + y_year + "-" + y_month;
                    createTimePlotChart(file_format + "-time.png", "X " + x_year + "." + x_month, "Y " + y_year + "." + y_month);

                    file_writer = File.CreateText(file_format + "-time.txt");

                    for (int k = 0; k < compare_count; k++)
                    {
                        file_writer.WriteLine(x_series_data[k] + " " + y_series_data[k]);
                    }

                    file_writer.Close();
                }
            }
        }

        private void x_file_open_btn_Click(object sender, EventArgs e)
        {
            x_pair_data = pairExtract(1);
        }

        private void y_file_open_btn_Click(object sender, EventArgs e)
        {
            y_pair_data = pairExtract(2);
        }

        private void x_month_classify_btn_Click(object sender, EventArgs e)
        {
            x_dynamic_pair_data = classifyPiarData(x_pair_data);
            MessageBox.Show("classification on x axis data is completed!");
        }

        private void y_month_classify_btn_Click(object sender, EventArgs e)
        {
            y_dynamic_pair_data = classifyPiarData(y_pair_data);
            MessageBox.Show("classification on y axis data is completed!");
        }

        private void xy_redata_btn_Click(object sender, EventArgs e)
        {
            int compare_count;

            for (int i = 0; i < x_dynamic_pair_data.Count; i++)
            {
                for (int j = 0; j < y_dynamic_pair_data.Count; j++)
                {
                    series_data = new List<double>();
                    x_series_data = new List<double>();
                    y_series_data = new List<double>();

                    compare_count = x_dynamic_pair_data[i].Count < y_dynamic_pair_data[j].Count ? x_dynamic_pair_data[i].Count : y_dynamic_pair_data[j].Count;

                    for (int k = 0; k < compare_count; k++)
                    {
                        series_data.Add(k);
                        x_series_data.Add(x_dynamic_pair_data[i][k].Value);
                        y_series_data.Add(y_dynamic_pair_data[j][k].Value);
                    }

                    /*
                    for (int k = 0; k < compare_count; k++)
                    {
                        series_data.Add(k);
                    }

                    foreach(KeyValuePair<DateTime, double> input_x_data in x_dynamic_pair_data[i])
                    {
                        x_series_data.Add(input_x_data.Value);
                    }

                    foreach (KeyValuePair<DateTime, double> input_y_data in y_dynamic_pair_data[i])
                    {
                        y_series_data.Add(input_y_data.Value);
                    }
                    */

                    int x_year = x_dynamic_pair_data[i][0].Key.Year;
                    int x_month = x_dynamic_pair_data[i][0].Key.Month;

                    int y_year = y_dynamic_pair_data[j][0].Key.Year;
                    int y_month = y_dynamic_pair_data[j][0].Key.Month;

                    string file_format = x_file_name + "-" + x_year + "-" + x_month + "&" + y_file_name + "-" + y_year + "-" + y_month;
                    createXYChart(file_format + ".png", x_file_name + " - X " + x_year + "." + x_month, y_file_name + " - Y " + y_year + "." + y_month);

                    file_writer = File.CreateText(file_format + ".txt");

                    for (int k = 0; k < compare_count; k++)
                    {
                        file_writer.WriteLine(x_series_data[k] + " " + y_series_data[k]);
                    }

                    file_writer.Close();
                }
            }
        }

        private void xy_random_walk_btn_Click(object sender, EventArgs e)
        {
            int compare_count;

            for (int i = 0; i < x_dynamic_pair_data.Count; i++)
            {
                for (int j = 0; j < y_dynamic_pair_data.Count; j++)
                {
                    x_series_data = new List<double>();
                    y_series_data = new List<double>();

                    List<double> x_data = new List<double>();
                    List<double> y_data = new List<double>();

                    compare_count = x_dynamic_pair_data[i].Count < y_dynamic_pair_data[j].Count ? x_dynamic_pair_data[i].Count : y_dynamic_pair_data[j].Count;

                    for (int k = 0; k < compare_count; k++)
                    {
                        x_data.Add(x_dynamic_pair_data[i][k].Value);
                        y_data.Add(y_dynamic_pair_data[j][k].Value);
                    }

                    int x_position = 0;
                    int y_position = 0;

                    x_series_data.Add(x_position);
                    y_series_data.Add(y_position);

                    for (int k = 0; k < compare_count - 1; k++)
                    {
                        int x_rate = 0;
                        int y_rate = 0;

                        if (x_data[k] == 0.0)
                        {
                            x_rate = X_CONST_MIDDLE_RATE;
                        }
                        else
                        {
                            x_rate = (int)((x_data[k + 1] / x_data[k]) * X_CONST_MIDDLE_RATE);
                        }

                        if (y_data[k] == 0.0)
                        {
                            y_rate = Y_CONST_MIDDLE_RATE;
                        }
                        else
                        {
                            y_rate = (int)((y_data[k + 1] / y_data[k]) * Y_CONST_MIDDLE_RATE);
                        }

                        if (x_rate <= X_CONST_MIDDLE_RATE - X_CONST_INTERVAL_RATE)
                        {
                            x_position--;
                        }
                        else if (x_rate >= X_CONST_MIDDLE_RATE + X_CONST_INTERVAL_RATE)
                        {
                            x_position++;
                        }

                        if (y_rate <= Y_CONST_MIDDLE_RATE - Y_CONST_INTERVAL_RATE)
                        {
                            y_position--;
                        }
                        else if (y_rate >= Y_CONST_MIDDLE_RATE + Y_CONST_INTERVAL_RATE)
                        {
                            y_position++;
                        }

                        x_series_data.Add(x_position);
                        y_series_data.Add(y_position);
                    }

                    int x_year = x_dynamic_pair_data[i][0].Key.Year;
                    int x_month = x_dynamic_pair_data[i][0].Key.Month;

                    int y_year = y_dynamic_pair_data[j][0].Key.Year;
                    int y_month = y_dynamic_pair_data[j][0].Key.Month;

                    string file_format = x_file_name + "-" + x_year + "-" + x_month + "&" + y_file_name + "-" + y_year + "-" + y_month;
                    createTimePlotChart(file_format + "-time.png", x_file_name + " - X " + x_year + "." + x_month, y_file_name + " - Y " + y_year + "." + y_month);

                    file_writer = File.CreateText(file_format + "-time.txt");

                    for (int k = 0; k < compare_count; k++)
                    {
                        file_writer.WriteLine(x_series_data[k] + " " + y_series_data[k]);
                    }

                    file_writer.Close();
                }
            }
        }        
    }
}
