﻿namespace SingleEconomic
{
    partial class DataminingForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataminingForm));
            this.c1Ribbon1 = new C1.Win.C1Ribbon.C1Ribbon();
            this.ribbonApplicationMenu1 = new C1.Win.C1Ribbon.RibbonApplicationMenu();
            this.ribbonConfigToolBar1 = new C1.Win.C1Ribbon.RibbonConfigToolBar();
            this.ribbonQat1 = new C1.Win.C1Ribbon.RibbonQat();
            this.ribbonTab1 = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup1 = new C1.Win.C1Ribbon.RibbonGroup();
            this.file_open_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup2 = new C1.Win.C1Ribbon.RibbonGroup();
            this.year_classify_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator1 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.month_classify_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator2 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.week_classify_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup3 = new C1.Win.C1Ribbon.RibbonGroup();
            this.redata_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonSeparator3 = new C1.Win.C1Ribbon.RibbonSeparator();
            this.random_walk_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonTab2 = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup4 = new C1.Win.C1Ribbon.RibbonGroup();
            this.x_file_open_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.y_file_open_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup5 = new C1.Win.C1Ribbon.RibbonGroup();
            this.x_month_classify_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.y_month_classify_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup6 = new C1.Win.C1Ribbon.RibbonGroup();
            this.xy_redata_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.xy_random_walk_btn = new C1.Win.C1Ribbon.RibbonButton();
            this.c1SplitContainer1 = new C1.Win.C1SplitContainer.C1SplitContainer();
            this.chart_panel = new C1.Win.C1SplitContainer.C1SplitterPanel();
            this.xyChart = new C1.Win.C1Chart.C1Chart();
            this.sub_container = new C1.Win.C1SplitContainer.C1SplitContainer();
            this.sub_panel = new C1.Win.C1SplitContainer.C1SplitterPanel();
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1SplitContainer1)).BeginInit();
            this.c1SplitContainer1.SuspendLayout();
            this.chart_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xyChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sub_container)).BeginInit();
            this.sub_container.SuspendLayout();
            this.SuspendLayout();
            // 
            // c1Ribbon1
            // 
            this.c1Ribbon1.ApplicationMenuHolder = this.ribbonApplicationMenu1;
            this.c1Ribbon1.ConfigToolBarHolder = this.ribbonConfigToolBar1;
            this.c1Ribbon1.Location = new System.Drawing.Point(0, 0);
            this.c1Ribbon1.Name = "c1Ribbon1";
            this.c1Ribbon1.QatHolder = this.ribbonQat1;
            this.c1Ribbon1.Size = new System.Drawing.Size(1233, 156);
            this.c1Ribbon1.Tabs.Add(this.ribbonTab1);
            this.c1Ribbon1.Tabs.Add(this.ribbonTab2);
            // 
            // ribbonApplicationMenu1
            // 
            this.ribbonApplicationMenu1.Name = "ribbonApplicationMenu1";
            // 
            // ribbonConfigToolBar1
            // 
            this.ribbonConfigToolBar1.Name = "ribbonConfigToolBar1";
            // 
            // ribbonQat1
            // 
            this.ribbonQat1.Name = "ribbonQat1";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Groups.Add(this.ribbonGroup1);
            this.ribbonTab1.Groups.Add(this.ribbonGroup2);
            this.ribbonTab1.Groups.Add(this.ribbonGroup3);
            this.ribbonTab1.Name = "ribbonTab1";
            this.ribbonTab1.Text = "Single";
            // 
            // ribbonGroup1
            // 
            this.ribbonGroup1.Items.Add(this.file_open_btn);
            this.ribbonGroup1.Name = "ribbonGroup1";
            this.ribbonGroup1.Text = "File";
            // 
            // file_open_btn
            // 
            this.file_open_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("file_open_btn.LargeImage")));
            this.file_open_btn.Name = "file_open_btn";
            this.file_open_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("file_open_btn.SmallImage")));
            this.file_open_btn.Text = "File Open";
            this.file_open_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.file_open_btn.Click += new System.EventHandler(this.file_open_btn_Click);
            // 
            // ribbonGroup2
            // 
            this.ribbonGroup2.Items.Add(this.year_classify_btn);
            this.ribbonGroup2.Items.Add(this.ribbonSeparator1);
            this.ribbonGroup2.Items.Add(this.month_classify_btn);
            this.ribbonGroup2.Items.Add(this.ribbonSeparator2);
            this.ribbonGroup2.Items.Add(this.week_classify_btn);
            this.ribbonGroup2.Name = "ribbonGroup2";
            this.ribbonGroup2.Text = "Classification";
            // 
            // year_classify_btn
            // 
            this.year_classify_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("year_classify_btn.LargeImage")));
            this.year_classify_btn.Name = "year_classify_btn";
            this.year_classify_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("year_classify_btn.SmallImage")));
            this.year_classify_btn.Text = "Year Classify";
            this.year_classify_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // ribbonSeparator1
            // 
            this.ribbonSeparator1.Name = "ribbonSeparator1";
            // 
            // month_classify_btn
            // 
            this.month_classify_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("month_classify_btn.LargeImage")));
            this.month_classify_btn.Name = "month_classify_btn";
            this.month_classify_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("month_classify_btn.SmallImage")));
            this.month_classify_btn.Text = "Month Classify";
            this.month_classify_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.month_classify_btn.Click += new System.EventHandler(this.month_classify_btn_Click);
            // 
            // ribbonSeparator2
            // 
            this.ribbonSeparator2.Name = "ribbonSeparator2";
            // 
            // week_classify_btn
            // 
            this.week_classify_btn.Enabled = false;
            this.week_classify_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("week_classify_btn.LargeImage")));
            this.week_classify_btn.Name = "week_classify_btn";
            this.week_classify_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("week_classify_btn.SmallImage")));
            this.week_classify_btn.Text = "Week Classify";
            this.week_classify_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            // 
            // ribbonGroup3
            // 
            this.ribbonGroup3.Items.Add(this.redata_btn);
            this.ribbonGroup3.Items.Add(this.ribbonSeparator3);
            this.ribbonGroup3.Items.Add(this.random_walk_btn);
            this.ribbonGroup3.Name = "ribbonGroup3";
            this.ribbonGroup3.Text = "Group";
            // 
            // redata_btn
            // 
            this.redata_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("redata_btn.LargeImage")));
            this.redata_btn.Name = "redata_btn";
            this.redata_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("redata_btn.SmallImage")));
            this.redata_btn.Text = "Data Regeneraion";
            this.redata_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.redata_btn.Click += new System.EventHandler(this.redata_btn_Click);
            // 
            // ribbonSeparator3
            // 
            this.ribbonSeparator3.Name = "ribbonSeparator3";
            // 
            // random_walk_btn
            // 
            this.random_walk_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("random_walk_btn.LargeImage")));
            this.random_walk_btn.Name = "random_walk_btn";
            this.random_walk_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("random_walk_btn.SmallImage")));
            this.random_walk_btn.Text = "Random Walk";
            this.random_walk_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.random_walk_btn.Click += new System.EventHandler(this.random_walk_btn_Click);
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Groups.Add(this.ribbonGroup4);
            this.ribbonTab2.Groups.Add(this.ribbonGroup5);
            this.ribbonTab2.Groups.Add(this.ribbonGroup6);
            this.ribbonTab2.Name = "ribbonTab2";
            this.ribbonTab2.Text = "Pair";
            // 
            // ribbonGroup4
            // 
            this.ribbonGroup4.Items.Add(this.x_file_open_btn);
            this.ribbonGroup4.Items.Add(this.y_file_open_btn);
            this.ribbonGroup4.Name = "ribbonGroup4";
            this.ribbonGroup4.Text = "File";
            // 
            // x_file_open_btn
            // 
            this.x_file_open_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("x_file_open_btn.LargeImage")));
            this.x_file_open_btn.Name = "x_file_open_btn";
            this.x_file_open_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("x_file_open_btn.SmallImage")));
            this.x_file_open_btn.Text = "X Data Open";
            this.x_file_open_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.x_file_open_btn.Click += new System.EventHandler(this.x_file_open_btn_Click);
            // 
            // y_file_open_btn
            // 
            this.y_file_open_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("y_file_open_btn.LargeImage")));
            this.y_file_open_btn.Name = "y_file_open_btn";
            this.y_file_open_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("y_file_open_btn.SmallImage")));
            this.y_file_open_btn.Text = "Y Data Open";
            this.y_file_open_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.y_file_open_btn.Click += new System.EventHandler(this.y_file_open_btn_Click);
            // 
            // ribbonGroup5
            // 
            this.ribbonGroup5.Items.Add(this.x_month_classify_btn);
            this.ribbonGroup5.Items.Add(this.y_month_classify_btn);
            this.ribbonGroup5.Name = "ribbonGroup5";
            this.ribbonGroup5.Text = "Classify";
            // 
            // x_month_classify_btn
            // 
            this.x_month_classify_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("x_month_classify_btn.LargeImage")));
            this.x_month_classify_btn.Name = "x_month_classify_btn";
            this.x_month_classify_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("x_month_classify_btn.SmallImage")));
            this.x_month_classify_btn.Text = "X Month Classify";
            this.x_month_classify_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.x_month_classify_btn.Click += new System.EventHandler(this.x_month_classify_btn_Click);
            // 
            // y_month_classify_btn
            // 
            this.y_month_classify_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("y_month_classify_btn.LargeImage")));
            this.y_month_classify_btn.Name = "y_month_classify_btn";
            this.y_month_classify_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("y_month_classify_btn.SmallImage")));
            this.y_month_classify_btn.Text = "Y Month Classify";
            this.y_month_classify_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.y_month_classify_btn.Click += new System.EventHandler(this.y_month_classify_btn_Click);
            // 
            // ribbonGroup6
            // 
            this.ribbonGroup6.Items.Add(this.xy_redata_btn);
            this.ribbonGroup6.Items.Add(this.xy_random_walk_btn);
            this.ribbonGroup6.Name = "ribbonGroup6";
            this.ribbonGroup6.Text = "Calculate";
            // 
            // xy_redata_btn
            // 
            this.xy_redata_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("xy_redata_btn.LargeImage")));
            this.xy_redata_btn.Name = "xy_redata_btn";
            this.xy_redata_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("xy_redata_btn.SmallImage")));
            this.xy_redata_btn.Text = "XY Redata";
            this.xy_redata_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.xy_redata_btn.Click += new System.EventHandler(this.xy_redata_btn_Click);
            // 
            // xy_random_walk_btn
            // 
            this.xy_random_walk_btn.LargeImage = ((System.Drawing.Image)(resources.GetObject("xy_random_walk_btn.LargeImage")));
            this.xy_random_walk_btn.Name = "xy_random_walk_btn";
            this.xy_random_walk_btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("xy_random_walk_btn.SmallImage")));
            this.xy_random_walk_btn.Text = "Pair Random Walk";
            this.xy_random_walk_btn.TextImageRelation = C1.Win.C1Ribbon.TextImageRelation.ImageAboveText;
            this.xy_random_walk_btn.Click += new System.EventHandler(this.xy_random_walk_btn_Click);
            // 
            // c1SplitContainer1
            // 
            this.c1SplitContainer1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.c1SplitContainer1.BorderColor = System.Drawing.Color.Black;
            this.c1SplitContainer1.BorderWidth = 1;
            this.c1SplitContainer1.CollapsingAreaColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(245)))), ((int)(((byte)(250)))));
            this.c1SplitContainer1.Dock = System.Windows.Forms.DockStyle.Left;
            this.c1SplitContainer1.FixedLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(166)))), ((int)(((byte)(194)))));
            this.c1SplitContainer1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            this.c1SplitContainer1.Location = new System.Drawing.Point(0, 156);
            this.c1SplitContainer1.Name = "c1SplitContainer1";
            this.c1SplitContainer1.Panels.Add(this.chart_panel);
            this.c1SplitContainer1.Size = new System.Drawing.Size(941, 758);
            this.c1SplitContainer1.SplitterColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(166)))), ((int)(((byte)(194)))));
            this.c1SplitContainer1.TabIndex = 2;
            this.c1SplitContainer1.ToolTipGradient = C1.Win.C1SplitContainer.ToolTipGradient.Blue;
            this.c1SplitContainer1.UseParentVisualStyle = false;
            // 
            // chart_panel
            // 
            this.chart_panel.Controls.Add(this.xyChart);
            this.chart_panel.Height = 100;
            this.chart_panel.Location = new System.Drawing.Point(1, 22);
            this.chart_panel.Name = "chart_panel";
            this.chart_panel.Size = new System.Drawing.Size(939, 735);
            this.chart_panel.TabIndex = 0;
            this.chart_panel.Text = "Chart Panel";
            // 
            // xyChart
            // 
            this.xyChart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.xyChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xyChart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            this.xyChart.Location = new System.Drawing.Point(0, 0);
            this.xyChart.Name = "xyChart";
            this.xyChart.PropBag = resources.GetString("xyChart.PropBag");
            this.xyChart.Size = new System.Drawing.Size(939, 735);
            this.xyChart.TabIndex = 0;
            // 
            // sub_container
            // 
            this.sub_container.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(221)))), ((int)(((byte)(238)))));
            this.sub_container.BorderColor = System.Drawing.Color.Black;
            this.sub_container.BorderWidth = 1;
            this.sub_container.CollapsingAreaColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(245)))), ((int)(((byte)(250)))));
            this.sub_container.Dock = System.Windows.Forms.DockStyle.Right;
            this.sub_container.FixedLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(166)))), ((int)(((byte)(194)))));
            this.sub_container.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            this.sub_container.Location = new System.Drawing.Point(946, 156);
            this.sub_container.Name = "sub_container";
            this.sub_container.Panels.Add(this.sub_panel);
            this.sub_container.Size = new System.Drawing.Size(287, 758);
            this.sub_container.SplitterColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(166)))), ((int)(((byte)(194)))));
            this.sub_container.TabIndex = 3;
            this.sub_container.ToolTipGradient = C1.Win.C1SplitContainer.ToolTipGradient.Blue;
            this.sub_container.UseParentVisualStyle = false;
            // 
            // sub_panel
            // 
            this.sub_panel.Height = 100;
            this.sub_panel.Location = new System.Drawing.Point(1, 22);
            this.sub_panel.Name = "sub_panel";
            this.sub_panel.Size = new System.Drawing.Size(285, 735);
            this.sub_panel.TabIndex = 0;
            this.sub_panel.Text = "Information Panel";
            // 
            // DataminingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1233, 914);
            this.Controls.Add(this.sub_container);
            this.Controls.Add(this.c1SplitContainer1);
            this.Controls.Add(this.c1Ribbon1);
            this.Name = "DataminingForm";
            this.Text = "Economic Datamining Form";
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1SplitContainer1)).EndInit();
            this.c1SplitContainer1.ResumeLayout(false);
            this.chart_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xyChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sub_container)).EndInit();
            this.sub_container.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private C1.Win.C1Ribbon.C1Ribbon c1Ribbon1;
        private C1.Win.C1Ribbon.RibbonApplicationMenu ribbonApplicationMenu1;
        private C1.Win.C1Ribbon.RibbonConfigToolBar ribbonConfigToolBar1;
        private C1.Win.C1Ribbon.RibbonQat ribbonQat1;
        private C1.Win.C1Ribbon.RibbonTab ribbonTab1;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup1;
        private C1.Win.C1Ribbon.RibbonButton file_open_btn;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup2;
        private C1.Win.C1Ribbon.RibbonButton month_classify_btn;
        private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator1;
        private C1.Win.C1Ribbon.RibbonButton week_classify_btn;
        private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator2;
        private C1.Win.C1Ribbon.RibbonButton year_classify_btn;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup3;
        private C1.Win.C1Ribbon.RibbonButton redata_btn;
        private C1.Win.C1SplitContainer.C1SplitContainer c1SplitContainer1;
        private C1.Win.C1SplitContainer.C1SplitterPanel chart_panel;
        private C1.Win.C1SplitContainer.C1SplitContainer sub_container;
        private C1.Win.C1SplitContainer.C1SplitterPanel sub_panel;
        private C1.Win.C1Chart.C1Chart xyChart;
        private C1.Win.C1Ribbon.RibbonSeparator ribbonSeparator3;
        private C1.Win.C1Ribbon.RibbonButton random_walk_btn;
        private C1.Win.C1Ribbon.RibbonTab ribbonTab2;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup4;
        private C1.Win.C1Ribbon.RibbonButton x_file_open_btn;
        private C1.Win.C1Ribbon.RibbonButton y_file_open_btn;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup5;
        private C1.Win.C1Ribbon.RibbonButton x_month_classify_btn;
        private C1.Win.C1Ribbon.RibbonButton y_month_classify_btn;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup6;
        private C1.Win.C1Ribbon.RibbonButton xy_redata_btn;
        private C1.Win.C1Ribbon.RibbonButton xy_random_walk_btn;
    }
}

